import { describe, test, expect, jest } from '@jest/globals'
import fs from 'fs'

import Routes from '../../src/routes.js'
import FileHelper from '../../src/fileHelper.js'

const fileHelper = new FileHelper()

describe('#FileHelper', () => {
  describe('#getFileStatus', () => {
    test('it should return files statuses in correct format', async () => {
      const statMock = {
        dev: 2064,
        mode: 33188,
        nlink: 1,
        uid: 1000,
        gid: 1000,
        rdev: 0,
        blksize: 4096,
        ino: 897260,
        size: 67298,
        blocks: 136,
        atimeMs: 1631188081830,
        mtimeMs: 1631188081820,
        ctimeMs: 1631188081820,
        birthtimeMs: 1631188081820,
        atime: '2021-09-09T11:48:01.830Z',
        mtime: '2021-09-09T11:48:01.820Z',
        ctime: '2021-09-09T11:48:01.820Z',
        birthtime: '2021-09-09T11:48:01.820Z'
      }
      const mockUser = 'fellipe-vidal'
      process.env.USER = mockUser
      const filename = 'file.png'

      jest
        .spyOn(fs.promises, fs.promises.readdir.name)
        .mockResolvedValue([filename])

      jest.spyOn(fs.promises, fs.promises.stat.name).mockResolvedValue(statMock)

      const result = await fileHelper.getFileStatus('/tmp')
      const expectedResult = [
        {
          size: '67.3 kB',
          lastModified: statMock.birthtime,
          owner: mockUser,
          file: filename
        }
      ]

      expect(fs.promises.stat).toHaveBeenCalledWith(`/tmp/${filename}`)
      expect(result).toMatchObject(expectedResult)
    })
  })
})
